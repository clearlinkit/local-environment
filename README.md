## Local Environment Setup

### Mac

The Mac setup is easy. Go to [https://docs.docker.com/docker-for-mac/install/](https://docs.docker.com/docker-for-mac/install/) and click on "Download from Docker Hub". I believe they require you to have user in order to get the software. Go ahead and sign up at no charge. Download and install. Now jump down to the **Configuration and Testing** section of this document.

### Windows

On windows there are a few more things we need to do. Most of this information I pulled from this blog post [https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly](https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly).

First go to "Turn Windows features on or off". You can do this by clicking on the windows icon and search for "Turn windows features on or off". Check the *Hyper-V* box. This will check all the sub-boxes as well for Hyper-V. If you haven't already installed WSL (Windows Subsystem for Linux), make sure you also check the *Windows Subsystem for Linux* box. Click *OK*. Windows will likely prompt you to restart. Go ahead and restart.

Now go to [https://docs.docker.com/docker-for-windows/install/](https://docs.docker.com/docker-for-windows/install/) and click on "Downoad from Docker Hub". You need a Docker Hub user, go ahead and sign up for free. Then download and install.

After installed make sure it's running. You should see in your system tray (bottom right) a little icon of a whale with containers on it's back. If not running you can search for it from your windows icon ("Docker Desktop") and click on it to get it running. When running and the icon is available in the system tray, right click and click on settings. Under the General tab check the box that says "Expose daemon on tcp://localhost:2375 without TLS".

#### WSL (Windows Subsystem for Linux)

Now we need to make sure everything is setup properly on the Linux side.

If you haven't already installed a linux distro for WSL then you'll need to click on your windows icon and search for "Microsoft Store". In the store search for your desired distro. WSL supports a few. I recommend Ubuntu. Right now when I search for "Ubuntu", 3 options show up. Just get the one without a version. It will install the latest available.

Open your Linux Bash and do the following to install Docker:

```bash

# Make sure all your packages are up to date
sudo apt-get update -y
sudo apt-get upgrade -y

# Install Docker's package dependencies.
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

# Download and add Docker's official public PGP key.
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Verify the fingerprint.
sudo apt-key fingerprint 0EBFCD88

# Add the `stable` channel's Docker upstream repository.
#
# If you want to live on the edge, you can change "stable" below to "test" or
# "nightly". I highly recommend sticking with stable!
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Update the apt package list (for the new apt repo).
sudo apt-get update -y

# Install the latest version of Docker CE.
sudo apt-get install -y docker-ce

# Allow your user to access the Docker CLI without needing root access.
sudo usermod -aG docker $USER

```

Then we need to install Docker Compose. Do the following:

```bash

# Install Python and PIP.
sudo apt-get install -y python3 python3-pip

# Install Docker Compose into your user's home directory.
pip3 install --user docker-compose

```

Next we need to make sure that $HOME/.local/bin is in your $PATH. You can view your path by typing `echo $PATH`. If it is not there, you can add it by opening your .profile file with your favorite linux editor (ie vi, vim, nano ...) and add this to the bottom: `PATH="$PATH:$HOME/.local/bin"`. Then run `source ~/.bashrc`.

Now you need to point your docker/docker-compose commands to go to the Docker service running on Windows. You can do this by adding a variable to your .bashrc file. You can do this by running the following:

`echo "export DOCKER_HOST=tcp://localhost:2375" >> ~/.bashrc && source ~/.bashrc`

Next we need to create a wsl config file to adjust how WSL mounts our disk drives. You need to create a file at /etc/wsl.conf with the following content:

```
[automount]
root = /
options = "metadata"
```

Then you need to log out of Windows and back in for this to take effect. Just locking your screen is not sufficient. You need to fully log out.

### Configuration and Testing

Make sure Docker Desktop is running. Open Docker settings. On the "Shared Drives" tab make sure to share any drives where your code is stored. Click "Apply".

Now let's make sure docker and docker-compose are working properly:

```bash
# You should get a bunch of output about your Docker daemon.
# If you get a permission denied error, close + open your terminal and try again.
docker info

# You should get back your Docker Compose version.
docker-compose --version
```

Now you need to make sure that **make** is installed on your machine. You can check if you already have it by typing `make -v`. If it's there, you should see some version output. If not, you'll need to install it. On WSL Ubuntu you can do this by typing `sudo apt-get install make -y`

To finish testing your setup, you'll need to clone this git repository. If you don't have a Bitbucket user, you'll need to create one. Using your favorite git client clone this repository on a drive you shared with Docker on your local machine. There is some specific information on how to clone this repository in the upper right corner of this page.

Once you have the code, navigate to the code in your terminal/WSL. Here you can simply type `make test` to test your environment setup. At this point test1 should pass and test2 should fail. Test2 fails because you need to have run your awscli authenticate that you learned about in the last training. Go ahead and authenticate and re-run the test.

If everything passes, you are ready for the next training!

If you have any questions or problems please reach out via email responding to the training email communications.

