
.PHONY: default test test1 test2

.DEFAULT:
	@echo 'Invalid target.'
	@echo '	test	runs all local environment tests'
	@echo '	test1	tests docker/docker-compose with a volume'
	@echo '	test2	tests docker build and aws-cli authentication and a cli command'

default: .DEFAULT

test: test1 test2
	@echo 'All tests ran successfully!!!'
	

test1:
	@echo 'Running test 1'
	docker-compose run --rm test1
	@echo 'Test 1 completed successfully!'


test2:
	@echo 'Running test 2'
	docker image build ./src -t local-env-test
	docker-compose run --rm test2
	@echo 'Test 2 completed successfully'
	@echo 'Removing test docker image'
	docker image remove local-env-test
	@echo 'Test docker image removed'